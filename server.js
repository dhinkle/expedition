//== Connect to Database
var mongoose = require('mongoose');
mongoose.connect('mongodb://expedition:dontknowwhatimdoing@ds051378.mongolab.com:51378/expedition');

//== Database Schema
var poem = mongoose.model('Poem', {
	title: String,
	author: String,
	body: String,
	dateCreated: { type: Date, default: Date.now },
	dateModified: { type: Date, default: Date.now }
});

/*
var kitty = new Cat({ title: 'Test1' });
kitty.save(function (err) {
  if (err) console.log('error: ', err);
  else console.log('meow');
});
*/

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/app'));

app.get('/api', function(req, res){
	res.send('Welcome to the expedition API');
});

/*
app.get('/', function(req, res){
  res.send('hello world');
});
*/

app.listen(4000);